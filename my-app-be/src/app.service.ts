import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getFibonacciCalculated(number: Number): Number {
    let n1 = 0, n2 = 1, nextTerm, finalValue = 0;
    let finalArray = [];

    console.log('Fibonacci Series:');

    for (let i = 1; i <= number; i++) {
      console.log(n1);
      finalArray.push(n1)
      nextTerm = n1 + n2;
      n1 = n2;
      n2 = nextTerm;
    }

    if (number == 0) {
      finalValue = 0
    } else if (number == 1) {
      finalValue = 1
    } else {
      finalValue = finalArray[finalArray.length - 1] + finalArray[finalArray.length - 2];
    }

    return finalValue;
  }
}
