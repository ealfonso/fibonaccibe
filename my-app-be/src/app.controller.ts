import { Controller, Get, HttpCode, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('api/fibonacci')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':number')
  @HttpCode(200)
  getFibonacciNumber(@Param('number') number: Number): String {
    if(number >= 0) {
    return "" + this.appService.getFibonacciCalculated(number);
    } else {
      return "Positive numbers are mandatory";
    }
  }
}
